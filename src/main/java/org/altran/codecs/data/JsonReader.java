package org.altran.codecs.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.io.Files;

import org.altran.codecs.IPluginInput;
import org.altran.codecs.model.CampaignBO;
import org.altran.codecs.model.EnvironmentBO;
import org.altran.codecs.model.TestBO;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class JsonReader implements Plugin<Project> {

	
	public String parseJsonFile(String jsonFilePath) throws FileNotFoundException, IOException, ParseException {

		// check json fileName
		isValidJsonFileName(jsonFilePath);
		JSONParser jsonP = new JSONParser();
		JSONObject jsonO = (JSONObject) jsonP.parse(new FileReader(jsonFilePath));
		return jsonO.toJSONString();
	}

	private boolean isValidJsonFileName(String jsonFilePath) {
		File file = new File(jsonFilePath.replace('\\', '/'));
		String name = file.getName();
		if ("json".equals(Files.getFileExtension(name)))
			return true;
		else
			return false;
	}

	public CampaignBO parseCampaignJsonFile(String file) throws FileNotFoundException, IOException, ParseException {

		JSONParser jsonP = new JSONParser();
		JSONObject jsonO = (JSONObject) jsonP.parse(new FileReader(file));

		CampaignBO campaignBo = new CampaignBO();
		campaignBo.setId((String) jsonO.get("ID"));
		campaignBo.setVersionJson((String) jsonO.get("JSON_Version"));
		campaignBo.setVersion((String) jsonO.get("Version"));
		campaignBo.setChangeLog((ArrayList<?>) jsonO.get("Change_log"));
		campaignBo.setCampaignType((String) jsonO.get("Campaign_type"));
		campaignBo.setAuthor((String) jsonO.get("Author"));
		campaignBo.setRetryCondition((String) jsonO.get("Retry_condition"));
		campaignBo.setTest((ArrayList<?>) jsonO.get("Test"));
		campaignBo.setCampaign((ArrayList<?>) jsonO.get("Campaign"));
		campaignBo.setEnvironment((String) jsonO.get("Environment"));
		campaignBo.setLogPath((String) jsonO.get("Log_path"));

		return campaignBo;

	}

	public TestBO parseTestJsonFile(String file) throws FileNotFoundException, IOException, ParseException {

		JSONParser jsonP = new JSONParser();
		JSONObject jsonO = (JSONObject) jsonP.parse(new FileReader(file));

		TestBO testBo = new TestBO();
		testBo.setId((String) jsonO.get("ID"));
		testBo.setVersionJson((String) jsonO.get("JSON_Version"));
		testBo.setVersion((String) jsonO.get("Version"));
		testBo.setChangeLog((ArrayList<?>) jsonO.get("Change_log"));
		testBo.setDescription((String) jsonO.get("Description"));
		testBo.setRequirement((ArrayList<?>) jsonO.get("Requirement"));
		testBo.setTestType((String) jsonO.get("Test_type"));
		testBo.setCriticity((String) jsonO.get("criticity"));
		testBo.setAuthor((String) jsonO.get("Author"));
		testBo.setInheritanceTestName((String) jsonO.get("Inheritance_test_Name"));
		testBo.setDependencies((ArrayList<?>) jsonO.get("Dependencies"));
		testBo.setPreConditionFunctions((ArrayList<?>) jsonO.get("Pre_condition_function"));
		testBo.setPostConditionFunctions((ArrayList<?>) jsonO.get("Post_condition_function"));
		testBo.setMaximuExecutionTime((Long) jsonO.get("Max_execution_time"));
		testBo.setRetryCondition((String) jsonO.get("Retry_condition"));
		testBo.setFunctionName((String) jsonO.get("Function_name"));
		testBo.setRangeValues((ArrayList<?>) jsonO.get("Range_values"));
		testBo.setExceptedResults((ArrayList<?>) jsonO.get("Expected_results"));
		testBo.setCustomerParameters((ArrayList<?>) jsonO.get("Customer_parameters"));

		return testBo;

	}

	public EnvironmentBO parseEnvironmentJsonFile(String file)
			throws FileNotFoundException, IOException, ParseException {

		JSONParser jsonP = new JSONParser();
		JSONObject jsonO = (JSONObject) jsonP.parse(new FileReader(file));

		EnvironmentBO environmentBo = new EnvironmentBO();
		environmentBo.setId((String) jsonO.get("ID"));
		environmentBo.setVersionJson((String) jsonO.get("JSON_Version"));
		environmentBo.setVersion((String) jsonO.get("Version"));
		environmentBo.setChangeLog((ArrayList<?>) jsonO.get("Change_log"));
		environmentBo.setDescription((String) jsonO.get("Description"));
		environmentBo.setEnvType((String) jsonO.get("Env_type"));
		environmentBo.setAuthor((String) jsonO.get("Author"));
		environmentBo.setTargetName((String) jsonO.get("Target_name"));
		environmentBo.setOsName((String) jsonO.get("OS_name"));
		environmentBo.setLittleIndian((boolean) jsonO.get("Little_indian"));
		environmentBo.setMaximumTargetThread((Long) jsonO.get("Maximum_target_thread"));
		environmentBo.setMaximumMemorySize((Long) jsonO.get("Maximum_memory_size"));
		environmentBo.setMaximumThreadMemorySize((Long) jsonO.get("Maximum_thread_memory_size"));
		environmentBo.setMaximumThread((Long) jsonO.get("Maximum_thread"));
		environmentBo.setThreadPriority((Long) jsonO.get("Thread_priority"));
		environmentBo.setMaximumExecutionTime((Long) jsonO.get("Maximum_execution_time"));
		environmentBo.setOutputPath((String) jsonO.get("Output_path"));
		environmentBo.setLogPath((String) jsonO.get("Log_path"));
		environmentBo.setCustomerParameters((ArrayList<?>) jsonO.get("Customer_parameters"));

		return environmentBo;

	}

	@Override
	public void apply(Project arg0) {
		// TODO Auto-generated method stub
		
	}

}
