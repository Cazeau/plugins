package org.altran.codecs;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

public interface IPluginInput {
	
	public String parseJsonFile(String jsonFile) throws FileNotFoundException, IOException, ParseException;
	
}
