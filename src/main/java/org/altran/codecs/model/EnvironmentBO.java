package org.altran.codecs.model;

import java.util.ArrayList;

public class EnvironmentBO {

	private String id;
    private String versionJson;
    private String version;
    private ArrayList<?> changeLog;
    private String description;
    private String envType;
    private String author;
    private String targetName;
    private String osName;
    private boolean littleIndian;
    private Long maximumTargetThread;
    private Long maximumMemorySize;
    private Long maximumThreadMemorySize;
    private Long maximumThread;
    private Long threadPriority;
    private Long maximumExecutionTime;
    private String outputPath;
    private String logPath;
    private ArrayList<?> customerParameters;
    
    
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the versionJson
	 */
	public String getVersionJson() {
		return versionJson;
	}
	/**
	 * @param versionJson the versionJson to set
	 */
	public void setVersionJson(String versionJson) {
		this.versionJson = versionJson;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the changeLog
	 */
	public ArrayList<?> getChangeLog() {
		return changeLog;
	}
	/**
	 * @param changeLog the changeLog to set
	 */
	public void setChangeLog(ArrayList<?> changeLog) {
		this.changeLog = changeLog;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the envType
	 */
	public String getEnvType() {
		return envType;
	}
	/**
	 * @param envType the envType to set
	 */
	public void setEnvType(String envType) {
		this.envType = envType;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the targetName
	 */
	public String getTargetName() {
		return targetName;
	}
	/**
	 * @param targetName the targetName to set
	 */
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	/**
	 * @return the osName
	 */
	public String getOsName() {
		return osName;
	}
	/**
	 * @param osName the osName to set
	 */
	public void setOsName(String osName) {
		this.osName = osName;
	}
	/**
	 * @return the littleIndian
	 */
	public boolean isLittleIndian() {
		return littleIndian;
	}
	/**
	 * @param littleIndian the littleIndian to set
	 */
	public void setLittleIndian(boolean littleIndian) {
		this.littleIndian = littleIndian;
	}
	/**
	 * @return the maximumTargetThread
	 */
	public Long getMaximumTargetThread() {
		return maximumTargetThread;
	}
	/**
	 * @param maximumTargetThread the maximumTargetThread to set
	 */
	public void setMaximumTargetThread(Long maximumTargetThread) {
		this.maximumTargetThread = maximumTargetThread;
	}
	/**
	 * @return the maximumMemorySize
	 */
	public Long getMaximumMemorySize() {
		return maximumMemorySize;
	}
	/**
	 * @param maximumMemorySize the maximumMemorySize to set
	 */
	public void setMaximumMemorySize(Long maximumMemorySize) {
		this.maximumMemorySize = maximumMemorySize;
	}
	/**
	 * @return the maximumThreadMemorySize
	 */
	public Long getMaximumThreadMemorySize() {
		return maximumThreadMemorySize;
	}
	/**
	 * @param maximumThreadMemorySize the maximumThreadMemorySize to set
	 */
	public void setMaximumThreadMemorySize(Long maximumThreadMemorySize) {
		this.maximumThreadMemorySize = maximumThreadMemorySize;
	}
	/**
	 * @return the maximumThread
	 */
	public Long getMaximumThread() {
		return maximumThread;
	}
	/**
	 * @param maximumThread the maximumThread to set
	 */
	public void setMaximumThread(Long maximumThread) {
		this.maximumThread = maximumThread;
	}
	/**
	 * @return the threadPriority
	 */
	public Long getThreadPriority() {
		return threadPriority;
	}
	/**
	 * @param threadPriority the threadPriority to set
	 */
	public void setThreadPriority(Long threadPriority) {
		this.threadPriority = threadPriority;
	}
	/**
	 * @return the maximumExecutionTime
	 */
	public Long getMaximumExecutionTime() {
		return maximumExecutionTime;
	}
	/**
	 * @param maximumExecutionTime the maximumExecutionTime to set
	 */
	public void setMaximumExecutionTime(Long maximumExecutionTime) {
		this.maximumExecutionTime = maximumExecutionTime;
	}
	/**
	 * @return the outputPath
	 */
	public String getOutputPath() {
		return outputPath;
	}
	/**
	 * @param outputPath the outputPath to set
	 */
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}
	/**
	 * @return the logPath
	 */
	public String getLogPath() {
		return logPath;
	}
	/**
	 * @param logPath the logPath to set
	 */
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	/**
	 * @return the customerParameters
	 */
	public ArrayList<?> getCustomerParameters() {
		return customerParameters;
	}
	/**
	 * @param customerParameters the customerParameters to set
	 */
	public void setCustomerParameters(ArrayList<?> customerParameters) {
		this.customerParameters = customerParameters;
	}
    

}
