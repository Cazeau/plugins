package org.altran.codecs.model;

import java.util.ArrayList;

public class TestBO {

	
	private String id;
    private String versionJson;
    private String version;
    private ArrayList<?> changeLog;
    private String description;
    private ArrayList<?> requirement;
    private String testType;
    private String criticity;
    private String author;
    private String inheritanceTestName;
    private ArrayList<?> dependencies;
    private ArrayList<?> preConditionFunctions;
    private ArrayList<?> postConditionFunctions;
    private Long maximuExecutionTime;
    private String retryCondition;
    private String functionName;
    private ArrayList<?> rangeValues;
    private ArrayList<?> ExceptedResults;
    private ArrayList<?> customerParameters;
    
    
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the versionJson
	 */
	public String getVersionJson() {
		return versionJson;
	}
	/**
	 * @param versionJson the versionJson to set
	 */
	public void setVersionJson(String versionJson) {
		this.versionJson = versionJson;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the changeLog
	 */
	public ArrayList<?> getChangeLog() {
		return changeLog;
	}
	/**
	 * @param changeLog the changeLog to set
	 */
	public void setChangeLog(ArrayList<?> changeLog) {
		this.changeLog = changeLog;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the requirement
	 */
	public ArrayList<?> getRequirement() {
		return requirement;
	}
	/**
	 * @param requirement the requirement to set
	 */
	public void setRequirement(ArrayList<?> requirement) {
		this.requirement = requirement;
	}
	/**
	 * @return the testType
	 */
	public String getTestType() {
		return testType;
	}
	/**
	 * @param testType the testType to set
	 */
	public void setTestType(String testType) {
		this.testType = testType;
	}
	/**
	 * @return the criticity
	 */
	public String getCriticity() {
		return criticity;
	}
	/**
	 * @param criticity the criticity to set
	 */
	public void setCriticity(String criticity) {
		this.criticity = criticity;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the inheritanceTestName
	 */
	public String getInheritanceTestName() {
		return inheritanceTestName;
	}
	/**
	 * @param inheritanceTestName the inheritanceTestName to set
	 */
	public void setInheritanceTestName(String inheritanceTestName) {
		this.inheritanceTestName = inheritanceTestName;
	}
	/**
	 * @return the dependencies
	 */
	public ArrayList<?> getDependencies() {
		return dependencies;
	}
	/**
	 * @param dependencies the dependencies to set
	 */
	public void setDependencies(ArrayList<?> dependencies) {
		this.dependencies = dependencies;
	}
	/**
	 * @return the preConditionFunctions
	 */
	public ArrayList<?> getPreConditionFunctions() {
		return preConditionFunctions;
	}
	/**
	 * @param preConditionFunctions the preConditionFunctions to set
	 */
	public void setPreConditionFunctions(ArrayList<?> preConditionFunctions) {
		this.preConditionFunctions = preConditionFunctions;
	}
	/**
	 * @return the postConditionFunctions
	 */
	public ArrayList<?> getPostConditionFunctions() {
		return postConditionFunctions;
	}
	/**
	 * @param postConditionFunctions the postConditionFunctions to set
	 */
	public void setPostConditionFunctions(ArrayList<?> postConditionFunctions) {
		this.postConditionFunctions = postConditionFunctions;
	}
	/**
	 * @return the maximuExecutionTime
	 */
	public Long getMaximuExecutionTime() {
		return maximuExecutionTime;
	}
	/**
	 * @param maximuExecutionTime the maximuExecutionTime to set
	 */
	public void setMaximuExecutionTime(Long maximuExecutionTime) {
		this.maximuExecutionTime = maximuExecutionTime;
	}
	/**
	 * @return the retryCondition
	 */
	public String getRetryCondition() {
		return retryCondition;
	}
	/**
	 * @param retryCondition the retryCondition to set
	 */
	public void setRetryCondition(String retryCondition) {
		this.retryCondition = retryCondition;
	}
	/**
	 * @return the functionName
	 */
	public String getFunctionName() {
		return functionName;
	}
	/**
	 * @param functionName the functionName to set
	 */
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	/**
	 * @return the rangeValues
	 */
	public ArrayList<?> getRangeValues() {
		return rangeValues;
	}
	/**
	 * @param rangeValues the rangeValues to set
	 */
	public void setRangeValues(ArrayList<?> rangeValues) {
		this.rangeValues = rangeValues;
	}
	/**
	 * @return the exceptedResults
	 */
	public ArrayList<?> getExceptedResults() {
		return ExceptedResults;
	}
	/**
	 * @param exceptedResults the exceptedResults to set
	 */
	public void setExceptedResults(ArrayList<?> exceptedResults) {
		ExceptedResults = exceptedResults;
	}
	/**
	 * @return the customerParameters
	 */
	public ArrayList<?> getCustomerParameters() {
		return customerParameters;
	}
	/**
	 * @param customerParameters the customerParameters to set
	 */
	public void setCustomerParameters(ArrayList<?> customerParameters) {
		this.customerParameters = customerParameters;
	}
    
    
	

}
