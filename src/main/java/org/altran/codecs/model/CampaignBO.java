package org.altran.codecs.model;

import java.util.ArrayList;

public class CampaignBO {
	
	private String id;
    private String versionJson;
    private String version;
    private ArrayList<?> changeLog;
    private String campaignType;
    private String author;
    private String retryCondition;
    private ArrayList<?> test;
    private ArrayList<?> campaign;
    private String environment;
    private String logPath;
    
    
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the versionJson
	 */
	public String getVersionJson() {
		return versionJson;
	}
	/**
	 * @param versionJson the versionJson to set
	 */
	public void setVersionJson(String versionJson) {
		this.versionJson = versionJson;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the changeLog
	 */
	public ArrayList<?> getChangeLog() {
		return changeLog;
	}
	/**
	 * @param changeLog the changeLog to set
	 */
	public void setChangeLog(ArrayList<?> changeLog) {
		this.changeLog = changeLog;
	}
	/**
	 * @return the campaignType
	 */
	public String getCampaignType() {
		return campaignType;
	}
	/**
	 * @param campaignType the campaignType to set
	 */
	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the retryCondition
	 */
	public String getRetryCondition() {
		return retryCondition;
	}
	/**
	 * @param retryCondition the retryCondition to set
	 */
	public void setRetryCondition(String retryCondition) {
		this.retryCondition = retryCondition;
	}
	/**
	 * @return the test
	 */
	public ArrayList<?> getTest() {
		return test;
	}
	/**
	 * @param test the test to set
	 */
	public void setTest(ArrayList<?> test) {
		this.test = test;
	}
	/**
	 * @return the campaign
	 */
	public ArrayList<?> getCampaign() {
		return campaign;
	}
	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(ArrayList<?> campaign) {
		this.campaign = campaign;
	}
	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}
	/**
	 * @param environment the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	/**
	 * @return the logPath
	 */
	public String getLogPath() {
		return logPath;
	}
	/**
	 * @param logPath the logPath to set
	 */
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
    
    

}
